const express = require('express');
const bodyParser = require('body-parser');
const app = express();

var jsonParser = bodyParser.json()
var urlencodedParser = bodyParser.urlencoded({ extended: false })

app.get('/',function(req,res){
        res.send("Hello World");
    })
    .get('/hello',function(req,res){
        if(req.query.nom){
            res.send("Bonjour, "+req.query.nom);
        }
        else{
            res.send("Quelle est votre nom ?");
        }
    })
    .post('/chat',jsonParser,function(req,res){
        if(req.body.msg === "ville"){
            res.send("Nous sommes à Paris");
        }
        else if(req.body.msg === "météo"){
            res.send("Il fait Beau");
        }
        else{
            res.send("Wrong mate ");
        }
        
    })
    .listen(process.env.PORT || 5000);